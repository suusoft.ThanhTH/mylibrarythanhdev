import XCTest
@testable import MyLibraryThanhDev

final class MyLibraryThanhDevTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(MyLibraryThanhDev().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
